Web Application interface
=========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pane_navigate
   pane_capture
   pane_settings
   pane_gallery

The main graphical interface for the OpenFlexure Microscope is implemented as a web application, which allows it to be accessed either through OpenFlexure Connect or a web browser.  See the :doc:`../quickstart` page for connection instructions.  A "tour" should guide users through the interface when they connect for the first time, and introduce the key interface elements.  

Interface structure
-------------------
The main interface has a tab bar on the left, which allows the operator to select different controls.  By default, the "view" pane does not show any additional controls, and a video feed from the camera fills the window.

.. image:: pane_view.png

Selecting one of the tab icons on the left will bring up the corresponding interface.  Most of the pages display the image on the right hand side, and add additional controls between the image display and the tab bar.

